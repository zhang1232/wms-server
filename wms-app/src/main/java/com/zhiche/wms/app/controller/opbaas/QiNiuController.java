package com.zhiche.wms.app.controller.opbaas;

import com.zhiche.wms.dto.base.ResultDTO;
import com.zhiche.wms.core.utils.qiniu.QiNiuUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "qiNiu-API", description = "七牛云服务接口")
@RestController
@RequestMapping("/qiNiu")
public class QiNiuController {


    /**
     * 获取上传凭证
     */
    @ApiOperation(value = "获取上传token", notes = "获取上传token,返回一个ResultDTO的json串")
    @GetMapping(value = "/uploadToken")
    public ResultDTO<Object> generateSimpleUploadToken() {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "获取成功");
        result.setData(QiNiuUtil.generateSimpleUploadToken());
        return result;
    }

    /**
     * 获取下载图片接口
     *
     * @param key 图片KEY
     */
    @ApiOperation(value = "获取图片下载地址", notes = "获取图片下载地址,返回一个ResultDTO的json串")
    @GetMapping(value = "/downloadUrl")
    public ResultDTO<Object> generateDownloadURL(String key) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "获取下载链接成功");
        result.setData(QiNiuUtil.generateDownloadURL(key, ""));
        return result;
    }
}
