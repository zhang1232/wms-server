package com.zhiche.wms.dto.opbaas.paramdto;

import java.io.Serializable;
import java.util.Date;

public class BuyShipmentParamWSDTO implements Serializable {

    private OrderReleaseParamWSDTO[] releases;

    /**
     * 指令号
     */
    private String shipmentGid;
    /**
     * 运输指令类型
     */
    private String attribute1;
    /**
     * 原指令单号
     */
    private String tariffOrganizationNumber;
    /**
     * 运输指令状态
     */
    private String userDefined1IconGid;
    /**
     * 调度配载时间
     */
    private Date insertDate;
    /**
     * 要求移车出库时间
     */
    private Date attributeDate2;
    /**
     * 实际移车出库时间
     */
    private Date attributeDate3;
    /**
     * 要求移车发运时间
     */
    private Date startTime;
    /**
     * 实际移车发运时间
     */
    private Date attributeDate1;
    /**
     * 要求移车抵达时间
     */
    private Date endTime;
    /**
     * 实际移车抵达时间
     */
    private Date attributeDate4;
    /**
     * 要求移车入库时间
     */
    private Date attributeDate5;
    /**
     * 实际移车入库时间
     */
    private Date attributeDate6;
    /**
     * 起运地编码
     */
    private String sourceLocationId;
    /**
     * 起运地名称
     */
    private String sourceLocationName;
    /**
     * 起运地省份
     */
    private String sourceProvince;
    /**
     * 起运地城市
     */
    private String sourceCity;
    /**
     * 目的地编码
     */
    private String destLocationId;
    /**
     * 目的地名称
     */
    private String destLocationName;
    /**
     * 目的地省份
     */
    private String destProvince;
    /**
     * 目的地城市
     */
    private String destCity;
    /**
     * 商品车数量
     */
    private Integer totalShipUnitCount;
    /**
     * 分供方编码
     */
    private String servprovGid;
    /**
     * 分供方
     */
    private String attribute10;
    /**
     * 隶属主体
     */
    private String attribute8;
    /**
     * 运输方式
     */
    private String transportModeGid;
    /**
     * 车牌号/车厢号/船号
     */
    private String powerUnitGid;
    /**
     * 挂车号
     */
    private String attribute9;
    /**
     * 司机编码
     */
    private String driverGid;
    /**
     * 司机姓名
     */
    private String attribute7;
    /**
     * 司机联系方式
     */
    private String attribute2;
    /**
     * 发运点数量
     */
    private Integer attributeNumber1;
    /**
     * 卸货点数量
     */
    private Integer attributeNumber2;
    /**
     * 空位数量
     */
    private Integer attributeNumber3;
    /**
     * 起运地区县
     */
    private String attribute3;
    /**
     * 起运地地址
     */
    private String attribute4;
    /**
     * 目的地区县
     */
    private String attribute5;
    /**
     * 目的地地址
     */
    private String attribute6;
    /**
     * 备注
     */
    private String attribute11;
    /**
     * 状态(10：正常，20：取消)
     */
    private String status;

    public OrderReleaseParamWSDTO[] getReleases() {
        return releases;
    }

    public void setReleases(OrderReleaseParamWSDTO[] releases) {
        this.releases = releases;
    }

    public String getShipmentGid() {
        return shipmentGid;
    }

    public void setShipmentGid(String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getTariffOrganizationNumber() {
        return tariffOrganizationNumber;
    }

    public void setTariffOrganizationNumber(String tariffOrganizationNumber) {
        this.tariffOrganizationNumber = tariffOrganizationNumber;
    }

    public String getUserDefined1IconGid() {
        return userDefined1IconGid;
    }

    public void setUserDefined1IconGid(String userDefined1IconGid) {
        this.userDefined1IconGid = userDefined1IconGid;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getAttributeDate2() {
        return attributeDate2;
    }

    public void setAttributeDate2(Date attributeDate2) {
        this.attributeDate2 = attributeDate2;
    }

    public Date getAttributeDate3() {
        return attributeDate3;
    }

    public void setAttributeDate3(Date attributeDate3) {
        this.attributeDate3 = attributeDate3;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getAttributeDate1() {
        return attributeDate1;
    }

    public void setAttributeDate1(Date attributeDate1) {
        this.attributeDate1 = attributeDate1;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getAttributeDate4() {
        return attributeDate4;
    }

    public void setAttributeDate4(Date attributeDate4) {
        this.attributeDate4 = attributeDate4;
    }

    public Date getAttributeDate5() {
        return attributeDate5;
    }

    public void setAttributeDate5(Date attributeDate5) {
        this.attributeDate5 = attributeDate5;
    }

    public Date getAttributeDate6() {
        return attributeDate6;
    }

    public void setAttributeDate6(Date attributeDate6) {
        this.attributeDate6 = attributeDate6;
    }

    public String getSourceLocationId() {
        return sourceLocationId;
    }

    public void setSourceLocationId(String sourceLocationId) {
        this.sourceLocationId = sourceLocationId;
    }

    public String getSourceLocationName() {
        return sourceLocationName;
    }

    public void setSourceLocationName(String sourceLocationName) {
        this.sourceLocationName = sourceLocationName;
    }

    public String getSourceProvince() {
        return sourceProvince;
    }

    public void setSourceProvince(String sourceProvince) {
        this.sourceProvince = sourceProvince;
    }

    public String getSourceCity() {
        return sourceCity;
    }

    public void setSourceCity(String sourceCity) {
        this.sourceCity = sourceCity;
    }

    public String getDestLocationId() {
        return destLocationId;
    }

    public void setDestLocationId(String destLocationId) {
        this.destLocationId = destLocationId;
    }

    public String getDestLocationName() {
        return destLocationName;
    }

    public void setDestLocationName(String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public Integer getTotalShipUnitCount() {
        return totalShipUnitCount;
    }

    public void setTotalShipUnitCount(Integer totalShipUnitCount) {
        this.totalShipUnitCount = totalShipUnitCount;
    }

    public String getServprovGid() {
        return servprovGid;
    }

    public void setServprovGid(String servprovGid) {
        this.servprovGid = servprovGid;
    }

    public String getAttribute10() {
        return attribute10;
    }

    public void setAttribute10(String attribute10) {
        this.attribute10 = attribute10;
    }

    public String getAttribute8() {
        return attribute8;
    }

    public void setAttribute8(String attribute8) {
        this.attribute8 = attribute8;
    }

    public String getTransportModeGid() {
        return transportModeGid;
    }

    public void setTransportModeGid(String transportModeGid) {
        this.transportModeGid = transportModeGid;
    }

    public String getPowerUnitGid() {
        return powerUnitGid;
    }

    public void setPowerUnitGid(String powerUnitGid) {
        this.powerUnitGid = powerUnitGid;
    }

    public String getAttribute9() {
        return attribute9;
    }

    public void setAttribute9(String attribute9) {
        this.attribute9 = attribute9;
    }

    public String getDriverGid() {
        return driverGid;
    }

    public void setDriverGid(String driverGid) {
        this.driverGid = driverGid;
    }

    public String getAttribute7() {
        return attribute7;
    }

    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }

    public Integer getAttributeNumber1() {
        return attributeNumber1;
    }

    public void setAttributeNumber1(Integer attributeNumber1) {
        this.attributeNumber1 = attributeNumber1;
    }

    public Integer getAttributeNumber2() {
        return attributeNumber2;
    }

    public void setAttributeNumber2(Integer attributeNumber2) {
        this.attributeNumber2 = attributeNumber2;
    }

    public Integer getAttributeNumber3() {
        return attributeNumber3;
    }

    public void setAttributeNumber3(Integer attributeNumber3) {
        this.attributeNumber3 = attributeNumber3;
    }

    public String getAttribute3() {
        return attribute3;
    }

    public void setAttribute3(String attribute3) {
        this.attribute3 = attribute3;
    }

    public String getAttribute4() {
        return attribute4;
    }

    public void setAttribute4(String attribute4) {
        this.attribute4 = attribute4;
    }

    public String getAttribute5() {
        return attribute5;
    }

    public void setAttribute5(String attribute5) {
        this.attribute5 = attribute5;
    }

    public String getAttribute6() {
        return attribute6;
    }

    public void setAttribute6(String attribute6) {
        this.attribute6 = attribute6;
    }

    public String getAttribute11() {
        return attribute11;
    }

    public void setAttribute11(String attribute11) {
        this.attribute11 = attribute11;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
