package com.zhiche.wms.dto.inbound;

import com.zhiche.wms.domain.model.inbound.InboundNoticeHeader;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class InboundNoticeHeaderDTO extends InboundNoticeHeader implements Serializable {
    private String driverMess;
    private String storeHouseName;
    private List<InboundNoticeDTO> inboundNoticeLines;

    public void addInboundNoticeLines(InboundNoticeDTO inboundNoticeDTO){
        if(Objects.isNull(inboundNoticeLines)){
            inboundNoticeLines= new ArrayList<>();
        }
        inboundNoticeLines.add(inboundNoticeDTO);
    }

    public String getDriverMess() {
        return driverMess;
    }

    public void setDriverMess(String driverMess) {
        this.driverMess = driverMess;
    }

    public String getStoreHouseName() {
        return storeHouseName;
    }

    public void setStoreHouseName(String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public List<InboundNoticeDTO> getInboundNoticeLines() {
        return inboundNoticeLines;
    }

    public void setInboundNoticeLines(List<InboundNoticeDTO> inboundNoticeLines) {
        this.inboundNoticeLines = inboundNoticeLines;
    }
}
