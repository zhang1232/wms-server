package com.zhiche.wms.dto.opbaas.resultdto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class InspectExcpDTO implements Serializable {
    private String taskNode;
    private String exDescribe;
    private String exStatus;
    private String missTaskNode;
    private String missStatus;
    private String componentDescribe;
    private String picKeys;
    private String excpId;

    public String getExcpId() {
        return excpId;
    }

    public void setExcpId(String excpId) {
        this.excpId = excpId;
    }

    public String getPicKeys() {
        return picKeys;
    }

    public void setPicKeys(String picKeys) {
        this.picKeys = picKeys;
    }

    public String getMissStatus() {
        return missStatus;
    }

    public void setMissStatus(String missStatus) {
        this.missStatus = missStatus;
    }

    public String getTaskNode() {
        return taskNode;
    }

    public void setTaskNode(String taskNode) {
        this.taskNode = taskNode;
    }

    public String getExDescribe() {
        return exDescribe;
    }

    public void setExDescribe(String exDescribe) {
        this.exDescribe = exDescribe;
    }

    public String getExStatus() {
        return exStatus;
    }

    public void setExStatus(String exStatus) {
        this.exStatus = exStatus;
    }

    public String getMissTaskNode() {
        return missTaskNode;
    }

    public void setMissTaskNode(String missTaskNode) {
        this.missTaskNode = missTaskNode;
    }

    public String getComponentDescribe() {
        return componentDescribe;
    }

    public void setComponentDescribe(String componentDescribe) {
        this.componentDescribe = componentDescribe;
    }
}
