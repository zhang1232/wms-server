package com.zhiche.wms.domain.model.opbaas;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * <p>
 * 发车点配置
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@TableName("op_delivery_point")
public class OpDeliveryPoint extends Model<OpDeliveryPoint> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
    /**
     * 发车点编码
     */
    private String code;
    /**
     * 发车点名称
     */
    private String name;
    /**
     * 所在省份
     */
    private String province;
    /**
     * 所在城市
     */
    private String city;
    /**
     * 所在区县
     */
    private String county;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 联系人
     */
    @TableField("link_man")
    private String linkMan;
    /**
     * 联系电话
     */
    @TableField("link_phone")
    private String linkPhone;
    /**
     * 需寻车
     */
    @TableField("is_seek")
    private String isSeek;
    /**
     * 用户是否需寻车
     */
    @TableField(exist = false)
    private boolean isSeekChecked;
    /**
     * 需移车
     */
    @TableField("is_move")
    private String isMove;
    /**
     * 用户是否需移车
     */
    @TableField(exist = false)
    private boolean isMoveChecked;
    /**
     * 需提车
     */
    @TableField("is_pick")
    private String isPick;
    /**
     * 用户是否需提车
     */
    @TableField(exist = false)
    private boolean isPickChecked;
    /**
     * 需发运
     */
    @TableField("is_ship")
    private String isShip;
    /**
     * 用户是否需发运
     */
    @TableField(exist = false)
    private boolean isShipChecked;
    /**
     * 状态(10：正常，20：失效)
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 用户是否需发运
     */
    @TableField(exist = false)
    private boolean isOwn;

    /**
     * 需发运
     */
    @TableField("exce_ship")
    private String exceShip;

    public String getExceShip() {
        return exceShip;
    }

    public void setExceShip(String exceShip) {
        this.exceShip = exceShip;
    }

    public String getIsShip() {
        return isShip;
    }

    public void setIsShip(String isShip) {
        this.isShip = isShip;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    public String getIsSeek() {
        return isSeek;
    }

    public void setIsSeek(String isSeek) {
        this.isSeek = isSeek;
    }

    public String getIsMove() {
        return isMove;
    }

    public void setIsMove(String isMove) {
        this.isMove = isMove;
    }

    public String getIsPick() {
        return isPick;
    }

    public void setIsPick(String isPick) {
        this.isPick = isPick;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public boolean getIsSeekChecked() {
        return Objects.isNull(isSeekChecked) ? false : isSeekChecked;
    }

    public void setIsSeekChecked(boolean isSeekChecked) {
        this.isSeekChecked = isSeekChecked;
    }

    public boolean getIsMoveChecked() {
        return Objects.isNull(isMoveChecked) ? false : isMoveChecked;
    }

    public void setIsMoveChecked(boolean isMoveChecked) {
        this.isMoveChecked = isMoveChecked;
    }

    public boolean getIsPickChecked() {
        return Objects.isNull(isPickChecked) ? false : isPickChecked;
    }

    public void setIsPickChecked(boolean isPickChecked) {
        this.isPickChecked = isPickChecked;
    }

    public boolean getIsShipChecked() {
        return Objects.isNull(isShipChecked) ? false : isShipChecked;
    }

    public void setIsShipChecked(boolean isShipChecked) {
        this.isShipChecked = isShipChecked;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public void setOwn(boolean own) {
        isOwn = own;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OpDeliveryPoint{" +
                ", id=" + id +
                ", code=" + code +
                ", name=" + name +
                ", province=" + province +
                ", city=" + city +
                ", county=" + county +
                ", address=" + address +
                ", linkMan=" + linkMan +
                ", linkPhone=" + linkPhone +
                ", isSeek=" + isSeek +
                ", isMove=" + isMove +
                ", isPick=" + isPick +
                ", status=" + status +
                ", remark=" + remark +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
