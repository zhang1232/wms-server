package com.zhiche.wms.domain.model.sys;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户发车点权限表
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
@TableName("sys_user_delivery_point")
public class UserDeliveryPoint extends Model<UserDeliveryPoint> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 发车点ID
     */
    @TableField("point_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pointId;
    /**
     * 寻车任务(1:是,0:否)
     */
    @TableField("is_seek")
    private Integer isSeek;
    /**
     * 移车任务(1:是,0:否)
     */
    @TableField("is_move")
    private Integer isMove;
    /**
     * 提车任务(1:是,0:否)
     */
    @TableField("is_pick")
    private Integer isPick;
    /**
     * 提车任务(1:是,0:否)
     */
    @TableField("is_ship")
    private String isShip;
    /**
     * 状态(1:正常,2:失效)
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    public String getIsShip() {
        return isShip;
    }

    public void setIsShip(String isShip) {
        this.isShip = isShip;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getPointId() {
        return pointId;
    }

    public void setPointId(Long pointId) {
        this.pointId = pointId;
    }

    public Integer getIsSeek() {
        return isSeek;
    }

    public void setIsSeek(Integer isSeek) {
        this.isSeek = isSeek;
    }

    public Integer getIsMove() {
        return isMove;
    }

    public void setIsMove(Integer isMove) {
        this.isMove = isMove;
    }

    public Integer getIsPick() {
        return isPick;
    }

    public void setIsPick(Integer isPick) {
        this.isPick = isPick;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "UserDeliveryPoint{" +
                ", id=" + id +
                ", userId=" + userId +
                ", pointId=" + pointId +
                ", isSeek=" + isSeek +
                ", isMove=" + isMove +
                ", isPick=" + isPick +
                ", status=" + status +
                ", remark=" + remark +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
