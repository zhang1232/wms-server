package com.zhiche.wms.domain.model.opbaas;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 二维码绑定
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@TableName("op_qrcode_bind")
public class OpQrCodeBind extends Model<OpQrCodeBind> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;
    /**
     * 运单号
     */
    @TableField("waybill_no")
    private String waybillNo;
    /**
     * OMS订单号
     */
    @TableField("oms_order_no")
    private String omsOrderNo;
    /**
     * VIN码
     */
    private String vin;
    /**
     * 二维码
     */
    @TableField("qr_code")
    private String qrCode;
    /**
     * 作业节点(10:验车,20:寻车,30:提车,40:入库,50:出库)
     */
    @TableField("task_node")
    private String taskNode;
    /**
     * 业务单据
     */
    @TableField("business_doc")
    private String businessDoc;
    /**
     * 司机编号
     */
    @TableField("driver_code")
    private String driverCode;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机电话
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 绑定时间
     */
    @TableField("bind_time")
    private Date bindTime;
    /**
     * 任务状态(10：正常，20：取消)
     */
    private String status;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWaybillNo() {
        return waybillNo;
    }

    public void setWaybillNo(String waybillNo) {
        this.waybillNo = waybillNo;
    }

    public String getOmsOrderNo() {
        return omsOrderNo;
    }

    public void setOmsOrderNo(String omsOrderNo) {
        this.omsOrderNo = omsOrderNo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getTaskNode() {
        return taskNode;
    }

    public void setTaskNode(String taskNode) {
        this.taskNode = taskNode;
    }

    public String getBusinessDoc() {
        return businessDoc;
    }

    public void setBusinessDoc(String businessDoc) {
        this.businessDoc = businessDoc;
    }

    public String getDriverCode() {
        return driverCode;
    }

    public void setDriverCode(String driverCode) {
        this.driverCode = driverCode;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date getBindTime() {
        return bindTime;
    }

    public void setBindTime(Date bindTime) {
        this.bindTime = bindTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OpQrCodeBind{" +
                ", id=" + id +
                ", waybillNo=" + waybillNo +
                ", omsOrderNo=" + omsOrderNo +
                ", vin=" + vin +
                ", qrCode=" + qrCode +
                ", taskNode=" + taskNode +
                ", businessDoc=" + businessDoc +
                ", driverCode=" + driverCode +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", bindTime=" + bindTime +
                ", status=" + status +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
