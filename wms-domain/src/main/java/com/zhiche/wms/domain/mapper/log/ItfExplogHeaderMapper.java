package com.zhiche.wms.domain.mapper.log;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.log.ItfExplogHeader;

/**
 * <p>
 * 接口导出日志头 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
public interface ItfExplogHeaderMapper extends BaseMapper<ItfExplogHeader> {

}
