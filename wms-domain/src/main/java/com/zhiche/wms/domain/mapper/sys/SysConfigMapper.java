package com.zhiche.wms.domain.mapper.sys;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.sys.SysConfig;

/**
 * <p>
 * WMS各功能节点配置 Mapper 接口
 * </p>
 *
 * @Date: Create in 17:00 2018/12/12
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
