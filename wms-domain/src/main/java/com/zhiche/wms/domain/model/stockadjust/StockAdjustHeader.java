package com.zhiche.wms.domain.model.stockadjust;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 库存调整单头
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@TableName("w_stock_adjust_header")
public class StockAdjustHeader extends Model<StockAdjustHeader> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<StockAdjustLine> stockAdjustLineList;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 仓库
     */
    @JsonSerialize(using=ToStringSerializer.class)
    @TableField("store_house_id")
    private Long storeHouseId;
    /**
     * 库存调整单号
     */
    @TableField("adjust_no")
    private String adjustNo;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 单据日期
     */
    @TableField("order_date")
    private Date orderDate;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 调整类型(10:盘点调整,20:破损调整)
     */
    @TableField("adjust_type")
    private String adjustType;
    /**
     * 状态(10:已保存,20:已审核,30:已撤销)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getAdjustNo() {
        return adjustNo;
    }

    public void setAdjustNo(String adjustNo) {
        this.adjustNo = adjustNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getAdjustType() {
        return adjustType;
    }

    public void setAdjustType(String adjustType) {
        this.adjustType = adjustType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "StockAdjustHeader{" +
                ", id=" + id +
                ", storeHouseId=" + storeHouseId +
                ", adjustNo=" + adjustNo +
                ", ownerId=" + ownerId +
                ", orderDate=" + orderDate +
                ", uom=" + uom +
                ", lineCount=" + lineCount +
                ", adjustType=" + adjustType +
                ", status=" + status +
                ", remarks=" + remarks +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }

    public List<StockAdjustLine> getStockAdjustLineList() {
        return stockAdjustLineList;
    }

    public void setStockAdjustLineList(List<StockAdjustLine> stockAdjustLineList) {
        this.stockAdjustLineList = stockAdjustLineList;
    }

    public void addStockAdjustLine(StockAdjustLine stockAdjustLine) {
        if (Objects.isNull(stockAdjustLineList)) {
            stockAdjustLineList = new ArrayList<>();
        }
        stockAdjustLineList.add(stockAdjustLine);
    }
}
