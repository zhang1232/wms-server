package com.zhiche.wms.domain.model.opbaas;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 缺件登记
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-07
 */
@TableName("missing_component_register")
public class MissingComponentRegister extends Model<MissingComponentRegister> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	private Long id;
    /**
     * 运单号
     */
	@TableField("waybill_no")
	private String waybillNo;
    /**
     * 运单状态
     */
	@TableField("waybill_status")
	private String waybillStatus;
    /**
     * 客户订单号
     */
	@TableField("oms_order_no")
	private String omsOrderNo;
    /**
     * VIN码
     */
	private String vin;
    /**
     * 二维码
     */
	@TableField("qr_code")
	private String qrCode;
    /**
     * 作业节点(0:指令,10:寻车,20:移车,30:提车,40:收车质检,41:入库移车,42:分配入库,50:出库备车,51:出库确认,60:装车交验)
     */
	@TableField("task_node")
	private String taskNode;
    /**
     * 业务单据
     */
	@TableField("business_doc")
	private String businessDoc;
    /**
     * 工具分类
     */
	@TableField("component_level1")
	private String componentLevel1;
    /**
     * 工具名称
     */
	@TableField("component_level2")
	private String componentLevel2;
    /**
     * 描述
     */
	@TableField("component_describe")
	private String componentDescribe;
    /**
     * 登记人
     */
	@TableField("register_user")
	private String registerUser;
    /**
     * 登记时间
     */
	@TableField("register_time")
	private Date registerTime;
    /**
     * 责任方
     */
	@TableField("involved_party")
	private String involvedParty;
    /**
     * 处理状态(10:未处理,20:处理中,30;关闭,40:让步)
     */
	@TableField("deal_status")
	private String dealStatus;
    /**
     * 处理方式(10:返厂维修,20:出库维修,30:库内维修,40:带伤发运) 
     */
	@TableField("deal_type")
	private String dealType;
    /**
     * 处理结果描述
     */
	@TableField("deal_result_desc")
	private String dealResultDesc;
    /**
     * 处理完成时间
     */
	@TableField("deal_end_time")
	private Date dealEndTime;
    /**
     * 状态(10：登记，20：处理，30：关闭)
     */
	private String status;
    /**
     * 创建人
     */
	@TableField("user_create")
	private String userCreate;
    /**
     * 修改人
     */
	@TableField("user_modified")
	private String userModified;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 修改时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWaybillNo() {
		return waybillNo;
	}

	public void setWaybillNo(String waybillNo) {
		this.waybillNo = waybillNo;
	}

	public String getWaybillStatus() {
		return waybillStatus;
	}

	public void setWaybillStatus(String waybillStatus) {
		this.waybillStatus = waybillStatus;
	}

	public String getOmsOrderNo() {
		return omsOrderNo;
	}

	public void setOmsOrderNo(String omsOrderNo) {
		this.omsOrderNo = omsOrderNo;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getTaskNode() {
		return taskNode;
	}

	public void setTaskNode(String taskNode) {
		this.taskNode = taskNode;
	}

	public String getBusinessDoc() {
		return businessDoc;
	}

	public void setBusinessDoc(String businessDoc) {
		this.businessDoc = businessDoc;
	}

	public String getComponentLevel1() {
		return componentLevel1;
	}

	public void setComponentLevel1(String componentLevel1) {
		this.componentLevel1 = componentLevel1;
	}

	public String getComponentLevel2() {
		return componentLevel2;
	}

	public void setComponentLevel2(String componentLevel2) {
		this.componentLevel2 = componentLevel2;
	}

	public String getComponentDescribe() {
		return componentDescribe;
	}

	public void setComponentDescribe(String componentDescribe) {
		this.componentDescribe = componentDescribe;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public String getInvolvedParty() {
		return involvedParty;
	}

	public void setInvolvedParty(String involvedParty) {
		this.involvedParty = involvedParty;
	}

	public String getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getDealResultDesc() {
		return dealResultDesc;
	}

	public void setDealResultDesc(String dealResultDesc) {
		this.dealResultDesc = dealResultDesc;
	}

	public Date getDealEndTime() {
		return dealEndTime;
	}

	public void setDealEndTime(Date dealEndTime) {
		this.dealEndTime = dealEndTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserCreate() {
		return userCreate;
	}

	public void setUserCreate(String userCreate) {
		this.userCreate = userCreate;
	}

	public String getUserModified() {
		return userModified;
	}

	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "MissingComponentRegister{" +
			", id=" + id +
			", waybillNo=" + waybillNo +
			", waybillStatus=" + waybillStatus +
			", omsOrderNo=" + omsOrderNo +
			", vin=" + vin +
			", qrCode=" + qrCode +
			", taskNode=" + taskNode +
			", businessDoc=" + businessDoc +
			", componentLevel1=" + componentLevel1 +
			", componentLevel2=" + componentLevel2 +
			", componentDescribe=" + componentDescribe +
			", registerUser=" + registerUser +
			", registerTime=" + registerTime +
			", involvedParty=" + involvedParty +
			", dealStatus=" + dealStatus +
			", dealType=" + dealType +
			", dealResultDesc=" + dealResultDesc +
			", dealEndTime=" + dealEndTime +
			", status=" + status +
			", userCreate=" + userCreate +
			", userModified=" + userModified +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
