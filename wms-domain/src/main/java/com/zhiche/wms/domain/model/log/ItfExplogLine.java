package com.zhiche.wms.domain.model.log;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 接口导出日志明细
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
@TableName("w_itf_explog_line")
public class ItfExplogLine extends Model<ItfExplogLine> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 日志头ID
     */
    @TableField("header_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long headerId;
    /**
     * 导出类型(10:入库导出,20:出库导出)
     */
    @TableField("export_type")
    private String exportType;
    /**
     * 相关数据ID
     */
    @TableField("relation_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long relationId;
    /**
     * 目标系统(10:ILS)
     */
    @TableField("target_source")
    private String targetSource;
    /**
     * 接口地址
     */
    @TableField("interface_url")
    private String interfaceUrl;
    /**
     * 导出开始时间
     */
    @TableField("export_start_time")
    private Date exportStartTime;
    /**
     * 导出完成时间
     */
    @TableField("export_end_time")
    private Date exportEndTime;
    /**
     * 导出状态(1:成功,0:失败)
     */
    @TableField("export_status")
    private String exportStatus;
    /**
     * 导出备注
     */
    @TableField("export_remarks")
    private String exportRemarks;
    /**
     * 数据内容
     */
    @TableField("data_content")
    private String dataContent;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField("request_id")
    private String requestId;

    /**
     * 操作人
     */
    @TableField("user_create")
    private String userCreate;


    @TableField(exist = false)
    private String inboundStatus;

    @TableField(exist = false)
    private Date inboundTime;

    @TableField(exist = false)
    private String shipmentGid;

    @TableField(exist = false)
    private String releaseGid;

    @TableField(exist = false)
    private String storeId;

    @TableField(exist = false)
    private String storeCode;

    @TableField(exist = false)
    private String vin;

    @TableField(exist = false)
    private String storeName;

    public String getStoreName () {
        return storeName;
    }

    public void setStoreName (String storeName) {
        this.storeName = storeName;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getStoreId () {
        return storeId;
    }

    public void setStoreId (String storeId) {
        this.storeId = storeId;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }


    public String getUserCreate () {
        return userCreate;
    }

    public void setUserCreate (String userCreate) {
        this.userCreate = userCreate;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("ItfExplogLine{");
        sb.append("id=").append(id);
        sb.append(", headerId=").append(headerId);
        sb.append(", exportType='").append(exportType).append('\'');
        sb.append(", relationId=").append(relationId);
        sb.append(", targetSource='").append(targetSource).append('\'');
        sb.append(", interfaceUrl='").append(interfaceUrl).append('\'');
        sb.append(", exportStartTime=").append(exportStartTime);
        sb.append(", exportEndTime=").append(exportEndTime);
        sb.append(", exportStatus='").append(exportStatus).append('\'');
        sb.append(", exportRemarks='").append(exportRemarks).append('\'');
        sb.append(", dataContent='").append(dataContent).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", requestId='").append(requestId).append('\'');
        sb.append(", inboundStatus='").append(inboundStatus).append('\'');
        sb.append(", inboundTime=").append(inboundTime);
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", storeId='").append(storeId).append('\'');
        sb.append(", storeCode='").append(storeCode).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", storeName='").append(storeName).append('\'');
        sb.append(", userCreate='").append(userCreate).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getStoreCode () {
        return storeCode;
    }

    public void setStoreCode (String storeCode) {
        this.storeCode = storeCode;
    }

    public String getInboundStatus () {
        return inboundStatus;
    }

    public void setInboundStatus (String inboundStatus) {
        this.inboundStatus = inboundStatus;
    }

    public Date getInboundTime () {
        return inboundTime;
    }

    public void setInboundTime (Date inboundTime) {
        this.inboundTime = inboundTime;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public String getExportType() {
        return exportType;
    }

    public void setExportType(String exportType) {
        this.exportType = exportType;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public String getTargetSource() {
        return targetSource;
    }

    public void setTargetSource(String targetSource) {
        this.targetSource = targetSource;
    }

    public String getInterfaceUrl() {
        return interfaceUrl;
    }

    public void setInterfaceUrl(String interfaceUrl) {
        this.interfaceUrl = interfaceUrl;
    }

    public Date getExportStartTime() {
        return exportStartTime;
    }

    public void setExportStartTime(Date exportStartTime) {
        this.exportStartTime = exportStartTime;
    }

    public Date getExportEndTime() {
        return exportEndTime;
    }

    public void setExportEndTime(Date exportEndTime) {
        this.exportEndTime = exportEndTime;
    }

    public String getExportStatus() {
        return exportStatus;
    }

    public void setExportStatus(String exportStatus) {
        this.exportStatus = exportStatus;
    }

    public String getExportRemarks() {
        return exportRemarks;
    }

    public void setExportRemarks(String exportRemarks) {
        this.exportRemarks = exportRemarks;
    }

    public String getDataContent() {
        return dataContent;
    }

    public void setDataContent(String dataContent) {
        this.dataContent = dataContent;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
