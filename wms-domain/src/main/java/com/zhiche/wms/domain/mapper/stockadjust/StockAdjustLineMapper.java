package com.zhiche.wms.domain.mapper.stockadjust;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustLine;

/**
 * <p>
 * 库存调整明细 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface StockAdjustLineMapper extends BaseMapper<StockAdjustLine> {

}
