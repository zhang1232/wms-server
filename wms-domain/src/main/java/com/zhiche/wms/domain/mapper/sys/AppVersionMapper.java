package com.zhiche.wms.domain.mapper.sys;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.sys.AppVersion;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-17
 */
public interface AppVersionMapper extends BaseMapper<AppVersion> {

}
