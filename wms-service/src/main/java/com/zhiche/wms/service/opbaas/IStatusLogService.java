package com.zhiche.wms.service.opbaas;


import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.StatusLog;

/**
 * <p>
 * 状态日志 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-11
 */
public interface IStatusLogService extends IService<StatusLog> {

}
