package com.zhiche.wms.service.common;


import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExceptionRegisterDetailDTO;

import java.util.List;

public interface CommonExceptionService {
    List<ExceptionRegisterDetailDTO> getExceptionInfo(ExceptionRegisterParamDTO dto);

    /**
     * 获取车架号或订单号对应总的异常数
     */
    int getExceptionTotal(ExceptionRegisterParamDTO dto);

    /**
     * 获取所有异常信息
     */
    Page<ExResultDTO> getAllExceptionPicUrl(ExceptionRegisterParamDTO dto);
}
