package com.zhiche.wms.service.opbaas.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.configuration.MyConfigurationProperties;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.InterfaceAddrEnum;
import com.zhiche.wms.core.supports.enums.NodeOptionEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.domain.mapper.opbaas.ExceptionRegisterMapper;
import com.zhiche.wms.domain.model.base.StorehouseNode;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionIsCanSendDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;
import com.zhiche.wms.service.opbaas.ExceptionToOTMService;
import com.zhiche.wms.service.opbaas.IDeliveryPointService;
import com.zhiche.wms.service.opbaas.IExceptionRegisterService;
import com.zhiche.wms.service.sys.IStorehouseNodeService;
import com.zhiche.wms.service.utils.CommonMethod;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 异常推送otm
 * </p>
 *
 * @author yzy
 * @since 2018-11-09
 */
@Service
public class ExceptionToOTMServiceImpl implements ExceptionToOTMService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionToOTMServiceImpl.class);

    @Autowired
    private IStorehouseNodeService storehouseNodeService;

    @Autowired
    private IExceptionRegisterService registerService;

    @Autowired
    private MyConfigurationProperties properties;

    @Autowired
    private IExceptionRegisterService exceptionRegisterService;

    @Autowired
    private IDeliveryPointService deliveryPointService;

    @Autowired
    private ExceptionRegisterMapper exceptionRegisterMapper;

    public void isCanSend(CommonConditionParamDTO conditionParamDTO){
        if (conditionParamDTO == null) {
            throw new BaseException("参数不能为空");
        }
        Map<String, String> condition = conditionParamDTO.getCondition();
        if (condition == null || condition.isEmpty()) {
            throw new BaseException("condition不能为空");
        }
        if (StringUtils.isBlank(condition.get("houseId"))) {
            throw new BaseException("仓库id不能为空");
        }
        if (StringUtils.isBlank(condition.get("lotNo1"))) {
            throw new BaseException("车架号不能为空");
        }
        if (StringUtils.isNotBlank(condition.get("isCanSend"))) {

            ExceptionRegister exceptionRegister = new ExceptionRegister();
            exceptionRegister.setHouseId(condition.get("houseId"));

            ExceptionIsCanSendDTO exceptionIsCanSendDTO = new ExceptionIsCanSendDTO();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            exceptionIsCanSendDTO.setRegister_time(simpleDateFormat.format(new Date()));
            exceptionIsCanSendDTO.setVin_code(condition.get("lotNo1"));
            String result = null;
            if (TableStatusEnum.STATUS_N.getCode().equals(condition.get("isCanSend"))) {
                exceptionIsCanSendDTO.setIs_send(TableStatusEnum.STATUS_N.getCode());
                exceptionRegister.setOtmStatus(TableStatusEnum.STATUS_N.getCode());
                LOGGER.info("异常处理推送otm 入参exceptionIsCanSendDTO{}",exceptionIsCanSendDTO.toString());
                result = HttpClientUtil.postJson(properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress(), null, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getSocketTimeOut());
                LOGGER.info("异常处理推送otm result:{},params:{},url:{}", result, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress());
            } else if (TableStatusEnum.STATUS_Y.getCode().equals(condition.get("isCanSend"))) {
                exceptionIsCanSendDTO.setIs_send(TableStatusEnum.STATUS_Y.getCode());
                exceptionRegister.setOtmStatus(TableStatusEnum.STATUS_Y.getCode());
                LOGGER.info("异常处理推送otm 入参 exceptionIsCanSendDTO{}",exceptionIsCanSendDTO.toString());
                result = HttpClientUtil.postJson(properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress(), null, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getSocketTimeOut());
                System.err.println(JSONObject.toJSONString(exceptionIsCanSendDTO));
                LOGGER.info("异常处理推送otm result:{},params:{},url:{}", result, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress());
            }
            JSONObject jsonObject = JSONObject.parseObject(result);
            if (jsonObject.getString("messageType").equals("success")) {
                Wrapper<ExceptionRegister> exceptionRegisterWrapper = new EntityWrapper<>();
                exceptionRegisterWrapper.eq("vin", condition.get("lotNo1"));
                registerService.update(exceptionRegister, exceptionRegisterWrapper);
            }
        }
    }

    public String isSend(String vin ,String originName) {
        Wrapper<ExceptionRegister> exceptionRegisterWrapper = new EntityWrapper<>();
        exceptionRegisterWrapper.eq("vin", vin);
        ExceptionRegister exceptionRegister = exceptionRegisterService.selectOne(exceptionRegisterWrapper);
        if (Objects.isNull(exceptionRegister)||StringUtils.isEmpty(exceptionRegister.getOtmStatus())) {
            Wrapper<OpDeliveryPoint> opDeliveryPointWrapper=new EntityWrapper<>();
            opDeliveryPointWrapper.eq("name",originName);
            OpDeliveryPoint opDeliveryPoint = deliveryPointService.selectOne(opDeliveryPointWrapper);
            if (!Objects.isNull(opDeliveryPoint)){
                if (!StringUtils.isEmpty(opDeliveryPoint.getExceShip())&&opDeliveryPoint.getExceShip().equals(TableStatusEnum.STATUS_N.getCode())) {
                    return  TableStatusEnum.STATUS_N.getCode();
                }
                if (!StringUtils.isEmpty(opDeliveryPoint.getExceShip())&&opDeliveryPoint.getExceShip().equals(TableStatusEnum.STATUS_Y.getCode())) {
                    return TableStatusEnum.STATUS_Y.getCode();
                }
            }
        } else {
            return exceptionRegister.getOtmStatus();
        }
        return null;
    }
    /**
     * 返厂维修数据推送OTM
     *
     * @param exceptionRegisters 异常发运表的数据
     * @param vin 车架号
     */
    @Override
    public void serviceFactoryToOTM (List<ExceptionRegister> exceptionRegisters, String vin) {
        Wrapper<ExResultDTO> ew = new EntityWrapper<>();
        ew.eq("a.vin", vin);
        ew.ne("a.status", TableStatusEnum.STATUS_0.getCode());
        List<ExResultDTO> resultDTOS = exceptionRegisterMapper.selectExListDetail(new Page<>(), ew);
        if (CollectionUtils.isNotEmpty(resultDTOS)) {
            for (ExResultDTO exResultDTO : resultDTOS) {
                Map<String, Object> param = new HashMap<>();
                param.put("vin", exResultDTO.getVin());
                param.put("exceptionResult", "返厂维修");
                param.put("exceptionDesc", exResultDTO.getExDescribe());
                param.put("registerTime", CommonMethod.getStringDate(exResultDTO.getRegisterTime()));
                param.put("photoUrl", exResultDTO.getPicKey());
                String jsonParam = JSONObject.toJSONString(param);
                String result = HttpClientUtil.postJson(properties.getOtmhost() + InterfaceAddrEnum.EXCEPTION_RTF_OTM.getAddress(), null,
                        jsonParam, properties.getSocketTimeOut());
                LOGGER.info("返厂维修推送otm result:{},params:{},url:{}", result, jsonParam, properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress());
                JSONObject jsonObject = JSONObject.parseObject(result);
                LOGGER.info("返厂维修推送OTM结果jsonObject：{}", jsonObject);
            }
        }
    }

}
