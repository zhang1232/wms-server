package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/11.
 */
public class LocationType {

    public static final String COMMON = "10"; //普通储位

    public static final String BIGGER = "20"; //大储位

    public static final String TEMP = "30";   //临时储位

    public static final String VIRTUAL = "40";//虚拟储位

}
