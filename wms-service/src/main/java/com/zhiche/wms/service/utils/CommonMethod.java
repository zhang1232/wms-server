package com.zhiche.wms.service.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:02 2019/4/23
 */
public class CommonMethod {

    /**
     *  过滤车架号的换行符
     * @param vin 车架号
     * @return 车架号数据
     */
    public static String[] setVins (String vin) {
        String[] split = vin.trim().replaceAll("\n",",").split(",");
        return split;
    }

    /**
     * 获取时间
     * @param date String类型的时间
     * @return 时间
     */
    public static String getStringDate (Date date) {
        //设置要获取到什么样的时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //获取String类型的时间
        return sdf.format(date);
    }


    private String stringToDate (String shipTime) {
        String resultDate = "";
        SimpleDateFormat sdftime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date date = sdftime.parse(shipTime);
            resultDate = sdf.format(date);
        } catch (ParseException e) {
            resultDate = sdf.format(new Date());
        }
        return resultDate;
    }

    public static String datetoString (Date shipTime) {
        String resultDate = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        resultDate = sdf.format(shipTime);
        return resultDate;
    }

}
