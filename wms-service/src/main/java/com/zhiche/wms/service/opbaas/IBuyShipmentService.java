package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.otm.OtmShipment;

/**
 * <p>
 * 调度指令 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface IBuyShipmentService extends IService<OtmShipment> {

}
