package com.zhiche.wms.service.sys;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.base.NodeOption;
import com.zhiche.wms.domain.model.base.StorehouseNode;

import java.util.List;

/**
 * <p>
 * 仓库信息 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IStorehouseNodeService extends IService<StorehouseNode> {


}
