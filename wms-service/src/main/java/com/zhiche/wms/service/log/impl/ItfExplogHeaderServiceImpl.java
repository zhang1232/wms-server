package com.zhiche.wms.service.log.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.log.ItfExplogHeaderMapper;
import com.zhiche.wms.domain.model.log.ItfExplogHeader;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.service.log.IItfExplogHeaderService;
import com.zhiche.wms.service.log.IItfExplogLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 接口导出日志头 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
@Service
public class ItfExplogHeaderServiceImpl extends ServiceImpl<ItfExplogHeaderMapper, ItfExplogHeader> implements IItfExplogHeaderService {

    @Autowired
    private IItfExplogLineService lineService;

    @Override
    public void insertWithLines(ItfExplogHeader exH) {
        List<ItfExplogLine> lines = exH.getItfExplogLines();
        exH.setExpCount(lines.size());
        for (ItfExplogLine line : lines) {
            lineService.insert(line);

        }
        insert(exH);
    }
}
