package com.zhiche.wms.service.base;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.base.BusinessDocNumber;

/**
 * <p>
 * 业务单据号 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-07
 */
public interface IBusinessDocNumberService extends IService<BusinessDocNumber> {

    String updateNextByProcedure(String prefix);

    /**
     * 根据前缀得到下一个序列号
     * @param prefix
     * @return
     */
    int getNextValue(String prefix);

    /**
     * 得到入库通知单号
     * @return
     */
    String getInboundNoticeNo();

    /**
     * 得到入库质检单号
     * @return
     */
    String getInboundInspectNo();

    /**
     * 得到入库单号
     * @return
     */
    String getInboundPutAwayNo();



    /**
     * 得到出库通知单号
     * @return
     */
    String getOutboundNoticeNo();

    /**
     * 得到出库质检单号
     * @return
     */
    String getOutboundPrepareNo();

    /**
     * 得到出库单号
     * @return
     */
    String getOutboundShipNo();

    /**
     * 得到移位单号
     * @return
     */
    String getMovementNo();

    /**
     * 得到期初库存单号
     * @return
     */
    String getStockInitNo();

    /**
     * 得到库存调整单号
     * @return
     */
    String getStockAdjustNo();

}
