package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/11.
 */
public class PutAwayGenMethod {

    /**
     * 手工创建
     */
    public static final String GEN_HAND = "10";

    /**
     * 扫码触发
     */
    public static final String GEN_SCAN = "20";

    /**
     * APP触发
     */
    public static final String GEN_APP = "30";

    /**
     * 道闸触发
     */
    public static final String GEN_BARRIER = "40";

    /**
     * RFID触发
     */
    public static final String GEN_RFID = "50";
}
