package com.zhiche.wms.service.webserivce.impl;

import com.zhiche.wms.dto.opbaas.paramdto.BuyShipmentParamWSDTO;
import com.zhiche.wms.service.webserivce.OpBuyShipmentWsService;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

/**
 * <p>
 * webservice 接口实现类
 * </p>
 */
@WebService(serviceName = "OpBuyShipmentWsService",
        targetNamespace = "http://webservice.wms.zhiche.com",
        endpointInterface = "com.zhiche.wms.service.webserivce.OpBuyShipmentWsService")
@Service
public class  OpBuyShipmentWsServiceImpl implements OpBuyShipmentWsService {
    @Override
    public String saveBuyShipment(BuyShipmentParamWSDTO[] buyShipments) {
        return "success";
    }

    @Override
    public String test() {
        return "success";
    }
}
