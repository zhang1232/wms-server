package com.zhiche.wms.service.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zhiche.wms.configuration.MyConfigurationProperties;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceAddrEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.service.dto.OTMEvent;
import com.zhiche.wms.service.dto.ProcessCallBack;
import com.zhiche.wms.service.dto.ShipParamDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by zhaoguixin on 2018/7/26.
 */
@Component
public class BusinessNodeExport {

    @Autowired
    private MyConfigurationProperties properties;

    /**
     * 导出事件至OTM
     *
     * @param otmEvent 事件
     * @return 请求ID
     */
    public String exportEventToOTM(OTMEvent otmEvent) {
        String result = HttpClientUtil.postJson(
                properties.getIntegrationhost() + InterfaceAddrEnum.EVENT_URI.getAddress(),
                null,
                JSON.toJSONString(otmEvent),
                properties.getSocketTimeOut());
        if (!StringUtils.isEmpty(result)) {
            RestfulResponse<String> restfulResponse = JSON.parseObject(result,
                    new TypeReference<RestfulResponse<String>>() {
                    });
            if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                return restfulResponse.getData();
            }
        }
        return null;
    }

    /**
     * 得到异步处理结果
     *
     * @param requestId 请求ID
     */
    public ProcessCallBack getProcessResult(String requestId) {
        Map<String, String> params = new HashMap<>();
        params.put("requestId", requestId);
        String result = HttpClientUtil.post(
                properties.getIntegrationhost() + InterfaceAddrEnum.PROCESS_RESULT_URI.getAddress(),
                params,
                properties.getSocketTimeOut());
        if (!StringUtils.isEmpty(result)) {
            RestfulResponse<ProcessCallBack> restfulResponse = JSON.parseObject(result,
                    new TypeReference<RestfulResponse<ProcessCallBack>>() {
                    });
            if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                return restfulResponse.getData();
            }
        }
        return null;
    }

    public String exportEventToOTMNew(ShipParamDTO paramDTO) {
        String result = HttpClientUtil.postJson(
                properties.getIntegrationhost() + InterfaceAddrEnum.EVENT_URI.getAddress(),
                null,
                JSON.toJSONString(paramDTO),
                properties.getSocketTimeOut());
        if (!StringUtils.isEmpty(result)) {
            RestfulResponse<String> restfulResponse = JSON.parseObject(result,
                    new TypeReference<RestfulResponse<String>>() {
                    });
            if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                return restfulResponse.getData();
            }
        }
        return null;
    }
}
