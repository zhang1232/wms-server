package com.zhiche.wms.admin.controller.store;


import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.model.base.StoreLocation;
import com.zhiche.wms.service.base.IStoreAreaService;
import com.zhiche.wms.service.base.IStoreLocationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 储位配置 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2018-08-15
 */
@Controller
@RequestMapping("/storeLocation")
public class StoreLocationController {

    @Autowired
    private IStoreLocationService storeLocationService;

    @PostMapping(value = "/queryLocationByPage")
    @ResponseBody
    public RestfulResponse<Page<StoreLocation>> queryLocationPage(@RequestBody Page<StoreLocation> page) {
        RestfulResponse<Page<StoreLocation>> result = new RestfulResponse<>(0, "success");
        Page<StoreLocation> resultPage = storeLocationService.getUsableLocationByPage(page);
        result.setData(resultPage);
        return result;
    }

    @PostMapping(value = "/addStoreLocation")
    @ResponseBody
    public RestfulResponse<String> addStoreLocation(@RequestBody StoreLocation storeLocation) {
        RestfulResponse<String> result = new RestfulResponse<>(0, "success");
        if (storeLocationService.getCountByCodeName(storeLocation) < 1) {
            boolean insert = storeLocationService.insert(storeLocation);
            if (insert) {
                return result;
            } else {
                result.setCode(-1);
                result.setMessage("新增库位失败，请重试");
                return result;
            }
        } else {
            result.setCode(-1);
            result.setMessage("库位已存在");
            return result;
        }
    }

    @PostMapping(value = "/updateStoreLocation")
    @ResponseBody
    public RestfulResponse<String> updateStoreLocation(@RequestBody StoreLocation storeLocation) {
        RestfulResponse<String> result = new RestfulResponse<>(0, "success");
        boolean update = storeLocationService.updateStoreLocation(storeLocation);
        if (update) {
            return result;
        } else {
            result.setCode(-1);
            result.setMessage("编辑库位失败，请重试");
            return result;
        }
    }

    /**
     * 导入期初库存 保存状态status20
     */
    @PostMapping("/importLocationInit")
    @ResponseBody
    public RestfulResponse<Map<String, JSONArray>> saveImportLocationInit(@RequestBody String data) {
        Map<String, JSONArray> result = storeLocationService.saveImportLocation(data);
        return new RestfulResponse<>(0, "请求成功", result);
    }

}

