package com.zhiche.wms.admin.controller.opbaas;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.dto.opbaas.resultdto.OpDeliveryPointResultDTO;
import com.zhiche.wms.service.opbaas.IDeliveryPointService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 发车点配置前端控制器
 */
@RestController
@RequestMapping("deliveryPoint")
@Api(value = "deliveryPoint", description = "发车点配置相关功能接口")
public class DeliveryPointController {

    @Autowired
    private IDeliveryPointService deliveryPointService;

    @ApiOperation(value = "listPoint", notes = "发车点配置列表查询")
    @ApiResponses(value = {
            @ApiResponse(code = 0, message = "查询成功", response = RestfulResponse.class),
            @ApiResponse(code = -1, message = "查询失败", response = RestfulResponse.class)
    })
    @PostMapping("/listPoint")
    public RestfulResponse<Page<OpDeliveryPointResultDTO>> listPoint(@RequestBody Page<OpDeliveryPointResultDTO> page) {
        Page<OpDeliveryPointResultDTO> data = deliveryPointService.listPointPage(page);
        return new RestfulResponse<>(0, "查询成功", data);
    }

    @ApiOperation(value = "allUsablePoint", notes = "全部可用发车点配置列表查询")
    @ApiResponses(value = {
            @ApiResponse(code = 0, message = "查询成功", response = RestfulResponse.class),
            @ApiResponse(code = -1, message = "查询失败", response = RestfulResponse.class)
    })
    @PostMapping("/allUsablePoint")
    public RestfulResponse<Page<OpDeliveryPoint>> allUsablePoint(@RequestBody Page<OpDeliveryPoint> page) {
        RestfulResponse<Page<OpDeliveryPoint>> result = new RestfulResponse<>(0, "success");
        try {
            result.setData(deliveryPointService.allUsablePoint(page));
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
        return result;
    }

    @ApiOperation(value = "savePoint", notes = "保存发车点配置")
    @ApiResponses(value = {
            @ApiResponse(code = 0, message = "保存成功", response = RestfulResponse.class),
            @ApiResponse(code = -1, message = "保存失败", response = RestfulResponse.class)
    })
    @PostMapping("/savePoint")
    public RestfulResponse<OpDeliveryPointResultDTO> savePoint(@RequestBody OpDeliveryPointResultDTO dto) {
        deliveryPointService.savePoint(dto);
        return new RestfulResponse<>(0, "保存成功", dto);
    }

    @ApiOperation(value = "updatePoint", notes = "更新发车点配置")
    @ApiResponses(value = {
            @ApiResponse(code = 0, message = "修改成功", response = RestfulResponse.class),
            @ApiResponse(code = -1, message = "修改失败", response = RestfulResponse.class)
    })
    @PostMapping("/updatePoint")
    public RestfulResponse<OpDeliveryPointResultDTO> updatePoint(@RequestBody OpDeliveryPointResultDTO dto) {
        deliveryPointService.updatePoint(dto);
        return new RestfulResponse<>(0, "修改成功", dto);
    }
}
