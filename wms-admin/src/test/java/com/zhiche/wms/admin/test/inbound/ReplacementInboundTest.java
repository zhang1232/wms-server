package com.zhiche.wms.admin.test.inbound;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.domain.mapper.log.ItfExplogLineMapper;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.service.common.IntegrationService;
import com.zhiche.wms.service.utils.BusinessNodeExport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Author: caiHua
 * @Description: 补发入库数据---> OTM
 * @Date: Create in 15:00 2019/3/13
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class ReplacementInboundTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReplacementInboundTest.class);

    public static final String USER = "admin";

    @Autowired
    private ItfExplogLineMapper itfExplogLineMapper;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private BusinessNodeExport nodeExport;

    @Test
    public void pushInboundToOtm () {
        //1、读取文件，获取未推送的车架号
        String vin = this.readFile();
        String[] split = vin.split(",");
        List<String> vins = Arrays.asList(split);
        LOGGER.info("读取文件结果vins.size:{} ,读取的数据为:{}", vins.size(),vins);
        //2、根据车架号查询日志表的json数据和url
        EntityWrapper<ItfExplogLine> ew = new EntityWrapper<>();
        ew.in("inl.lot_no1", vins);
        ew.eq("iel.export_type", "BS_WMS_IN");
        ew.eq("inl.STATUS", TableStatusEnum.STATUS_30.getCode());
//        ew.eq("inh.store_house_id","10008");
//        ew.isNull("iel.request_id");
        List<ItfExplogLine> itfExplogLines = itfExplogLineMapper.queryPushInboundToOtm(null, ew);
        LOGGER.info("需要补发入库数据查询结果{}", itfExplogLines);
        if (CollectionUtils.isNotEmpty(itfExplogLines)) {
            for (ItfExplogLine itfExplogLine : itfExplogLines) {
                //3、推送数据至OTM
                LOGGER.info("补发入库数据推送OTM入参：{}", itfExplogLine.getDataContent());
                String result = HttpClientUtil.postJson(itfExplogLine.getInterfaceUrl(),
                        null, itfExplogLine.getDataContent(),
                        60000);
                if (!StringUtils.isEmpty(result)) {
                    RestfulResponse<String> restfulResponse = JSON.parseObject(result,
                            new TypeReference<RestfulResponse<String>>() {
                            });
                    if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                        LOGGER.info("补发入库数据推送OTM返回数据res：{}", restfulResponse.getData());
                        ItfExplogLine param = new ItfExplogLine();
                        param.setRequestId(restfulResponse.getData());
                        EntityWrapper<ItfExplogLine> itfExplogLineEntityWrapper = new EntityWrapper<>();
                        itfExplogLineEntityWrapper.eq("id", itfExplogLine.getId());
                        itfExplogLineMapper.update(param, itfExplogLineEntityWrapper);
                    }
                }
            }
        }
    }

    /**
     * 读取文件获取车架号
     *
     * @return
     */
    private String readFile () {
        StringBuffer vins = new StringBuffer();
        String pathname = "D://inboundVin.txt";
        try {
            FileReader reader = new FileReader(pathname);
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                // 一次读入一行数据
                LOGGER.info("循环读取每一行数据：{}", line);
                vins.append(line);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return vins.toString();
    }

}
